package com.adidas.config;

public class ServicesConfiguration {
    // Base URI
    public static final String URI = "http://petstore.swagger.io/v2";

    // Endpoints
    public static final String PET = "/pet";
    public static final String ORDER_STORE = "/store/order";
    public static final String INVENTORY_STORE = "/store/inventory";
    public static final String USER = "/user";
}
