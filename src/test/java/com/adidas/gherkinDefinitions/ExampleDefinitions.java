package com.adidas.gherkinDefinitions;

import com.adidas.serenitySteps.ExampleSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class ExampleDefinitions {
    @Steps
    private ExampleSteps exampleSteps;

    @When("^I request to (get|delete) a pet by ID \"([^\"]*)\"$")
    public void iRequestOperPetByID(String operation, String id) {
        switch (operation.toLowerCase()) {
            case "get":
                exampleSteps.getPetById(id);
                break;
            case "delete":
                exampleSteps.deletePetById(id);
                break;
            default:
                break;
        }
    }

    @When("^I request to (get|delete|update) a user by username \"([^\"]*)\"$")
    public void iRequestOperUserByID(String operation, String id) {
        switch (operation.toLowerCase()) {
            case "get":
                exampleSteps.getUserByUsername(id);
                break;
            case "delete":
                exampleSteps.deleteUserByUsername(id);
                break;
            case "update":
                exampleSteps.updateUserByUsername(id);
                break;
            default:
                break;
        }
    }

    @When("I request to create a pet")
    public void iRequestToCreateNewPet() {
        exampleSteps.createPet();
    }

    @When("I request to create a user")
    public void iRequestToCreateNewUser() {
        exampleSteps.createUser();
    }

    @When("I request to update a pet")
    public void iRequestToUpdatePet() {
        exampleSteps.updatePet();
    }

    @Then("I should get (.*) status code")
    public void iShouldGetStatusCode(int expectedStatusCode) {
        exampleSteps.verifyStatusCode(expectedStatusCode);
    }

    @And("^The value for the \"([^\"]*)\" after (get|post|update|delete) operation should be \"([^\"]*)\"$")
    public void theValueForTheAfterGetOperationShouldBe(String key, String operation, String expectedValue) {
        Response res = Serenity.sessionVariableCalled("response");
        exampleSteps.verifyValueFromKey(res, operation, key, expectedValue);
    }

    @When("I request to place an order for a pet")
    public void iRequestToPlaceAHolder(){
        exampleSteps.placeHolder();
    }

    @When("^I request to (get|delete) a purchase order by ID \"([^\"]*)\"$")
    public void iRequestOperOrderPurchaseByID(String operation, String id) {
        switch (operation.toLowerCase()) {
            case "get":
                exampleSteps.getPetPurchaseOrderById(id);
                break;
            case "delete":
                exampleSteps.deletePurchaseOrderById(id);
                break;
            default:
                break;
        }
    }

    @When("I request to get the inventory by status")
    public void iRequestToGetInventory(){
        exampleSteps.getInventory();
    }

}
