package com.adidas.serenitySteps;

import com.adidas.config.ServicesConfiguration;
import com.adidas.support.ServicesSupport;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import org.json.JSONObject;
import org.junit.Assert;

import java.io.InputStream;

import static net.serenitybdd.rest.SerenityRest.rest;

public class ExampleSteps {
    private ServicesSupport servicesSupport = new ServicesSupport();
    private RequestSpecification spec = rest().baseUri(ServicesConfiguration.URI).contentType(ContentType.JSON).when();

    public String getPetEndpoint() {
        return petEndpoint;
    }

    public String getOrderStoreEndpoint() {
        return orderStoreEndpoint;
    }

    public String getInventoryStoreEndpoint() {
        return inventoryStoreEndpoint;
    }

    public String getUserEndpoint() {
        return userEndpoint;
    }

    private String petEndpoint = ServicesConfiguration.PET;
    private String orderStoreEndpoint = ServicesConfiguration.ORDER_STORE;
    private String inventoryStoreEndpoint = ServicesConfiguration.INVENTORY_STORE;
    private String userEndpoint = ServicesConfiguration.USER;

    /**
     * Performs a GET operation with an ID provided by parameter from the scenario
     * @param id The ID of a pet
     * */
    @Step
    public void getPetById(String id) {

        String petEndpoint = getPetEndpoint() + "/" + id;
        Response response = servicesSupport.executeRequest(spec,"GET", petEndpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Performs a GET operation with an ID provided by parameter from the scenario
     * @param id The ID of a user
     * */
    @Step
    public void getUserByUsername(String id) {

        String userEndpoint = getUserEndpoint() + "/" + id;
        Response response = servicesSupport.executeRequest(spec,"GET", userEndpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Performs a POST operation that will create a new pet
     * */
    @Step
    public void createPet(){

        try {
            InputStream is = this.getClass().getResourceAsStream("/requests/create_pet.json");
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            spec = spec.body(body.toMap());
//            String req = IOUtils.toString(is, "UTF-8");
//            spec = spec.body(req);
            Response response = servicesSupport.executeRequest(spec,"POST", petEndpoint);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    /**
     * Performs a POST operation that will create a new user
     **/
    @Step
    public void createUser(){

        try {
            InputStream is = this.getClass().getResourceAsStream("/requests/create_userr.json");
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            spec = spec.body(body.toMap());
//            String req = IOUtils.toString(is, "UTF-8");
//            spec = spec.body(req);
            Response response = servicesSupport.executeRequest(spec,"POST", userEndpoint);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    /**
     * Performs a PUT operation without an ID from the scenario as a parameter
     * */
    @Step
    public void updatePet() {

        try {
            InputStream is = this.getClass().getResourceAsStream("/requests/update_pet.json");
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            spec = spec.body(body.toMap());
//            String req = IOUtils.toString(is, "UTF-8");
//            spec = spec.body(req);
            Response response = servicesSupport.executeRequest(spec,"PUT", petEndpoint);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    /**
     * Performs a PUT operation without an ID from the scenario as a parameter
     * @param id The ID of a user
     * */
    @Step
    public void updateUserByUsername(String id) {

        try {
            String userEndpoint = getUserEndpoint() + "/" + id;
            InputStream is = this.getClass().getResourceAsStream("/requests/update_userr.json");
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            spec = spec.body(body.toMap());
            Response response = servicesSupport.executeRequest(spec,"PUT", userEndpoint);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    /**
     * Performs a DELETE operation with an ID provided by parameter from the scenario
     * @param id The ID of a pet
     * */
    @Step
    public void deletePetById(String id) {

        String petEndpoint = getPetEndpoint() + "/" + id;
        Response response = servicesSupport.executeRequest(spec,"DELETE", petEndpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Performs a DELETE operation with an ID provided by parameter from the scenario
     * @param id The ID of a pet
     * */
    @Step
    public void deleteUserByUsername(String id) {

        String userEndpoint = getUserEndpoint() + "/" + id;
        Response response = servicesSupport.executeRequest(spec,"DELETE", userEndpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Method to verify an status code received from the scenario
     * @param expectedStatusCode Expected status code in the response
     * */
    @Step
    public void verifyStatusCode(int expectedStatusCode){

        Response res = Serenity.sessionVariableCalled("response");
        Assert.assertEquals("status code doesn't match", expectedStatusCode, res.getStatusCode());
    }

    /**
     * Method to verify an status code received from the scenario
     * @param res Response object from a previous operation
     * @param operation The operation that was done in a previous step received from Cucumber
     * @param key Attribute name received from the scenario as a parameter
     * @param expectedValue Expected value of the attribute received from the scenario as a parameter
     * */
    @Step
    public void verifyValueFromKey(Response res, String operation, String key, String expectedValue) {

        String currentValue = "";

        switch (operation.toLowerCase()) {
            case "get":
                currentValue = res.getBody().jsonPath().getString(key);
                break;
            case "post":
                currentValue = res.getBody().jsonPath().getString(key);
                break;
            case "update":
                currentValue = res.getBody().jsonPath().getString(key);
                break;
            case "delete":
                currentValue = res.getBody().jsonPath().getString(key);
                break;
            default:
                break;
        }

        Assert.assertEquals("Value for " + key + " doesn't match", expectedValue, currentValue);
    }


    /**
     * Method to place a holder for a pet in the store

     * */
    @Step
    public void placeHolder(){
        try {
            InputStream is = this.getClass().getResourceAsStream("/requests/order_pet.json");
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            spec = spec.body(body.toMap());
//            String req = IOUtils.toString(is, "UTF-8");
//            spec = spec.body(req);
            Response response = servicesSupport.executeRequest(spec,"POST", orderStoreEndpoint);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }

    }

    @Step
    public void getPetPurchaseOrderById(String id){
        String orderStoreEndpoint = getOrderStoreEndpoint() +"/" + id;
        Response response = servicesSupport.executeRequest(spec,"GET", orderStoreEndpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    @Step
    public void deletePurchaseOrderById(String id){
        String orderStoreEndpoint = getOrderStoreEndpoint() +"/" + id;
        Response response = servicesSupport.executeRequest(spec,"DELETE", orderStoreEndpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    @Step
    public void getInventory(){
        Response response = servicesSupport.executeRequest(spec,"GET", inventoryStoreEndpoint);
        Serenity.setSessionVariable("response").to(response);
    }
}
