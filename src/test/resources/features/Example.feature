@rr
Feature: Basic API feature

  Scenario Outline: Get an existing pet
    When I request to get a pet by ID "<petID>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after get operation should be "<value>"

    Examples:
      | petID | key        | value  | expectedStatusCode |
      | 8912  | name       | doggie | 200                |


  Scenario Outline: Create a new pet successfully
    When I request to create a pet
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after post operation should be "<value>"

    Examples:
      | key  | value    | expectedStatusCode |
      | name | Rodrigo  | 200                |

  Scenario Outline: Update an existing pet
    When I request to update a pet
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after update operation should be "<value>"

    Examples:
      | key  | value    | expectedStatusCode |
      | name | doggie   | 200                |

  Scenario: Delete an existing pet
    When I request to delete a pet by ID "8913"
    Then I should get 200 status code

  Scenario Outline: Create a new user successfully
    When I request to create a user
    Then I should get <expectedStatusCode> status code

    Examples:
      | expectedStatusCode |
      | 200                |

  Scenario Outline: Get an existing user by username
    When I request to get a user by username "<valuee>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after get operation should be "<value>"

    Examples:
      | valuee | key        | value  | expectedStatusCode |
      | julia  | username   | julia  | 200                |

  Scenario Outline: Get an existing user by username
    When I request to get a user by username "<valuee>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after get operation should be "<value>"

    Examples:
      | valuee | key        | value  | expectedStatusCode |
      | julio  | username   | julio  | 200                |


  Scenario Outline: Update an existing user by username
    When I request to update a user by username "<username>"
    Then I should get <expectedStatusCode> status code

    Examples:
      | expectedStatusCode | username |
      | 200                | julia    |

  Scenario Outline: Delete an existing user by username
    When I request to delete a user by username "<username>"
    Then I should get 200 status code

    Examples:
      | username  |
      | julia     |

  Scenario Outline: Place an order for a pet
    When I request to place an order for a pet
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after post operation should be "<value>"

    Examples:
      | expectedStatusCode | key  | value   |
      | 200                | id   | 8       |

  Scenario Outline: Find purchase order by ID
    When I request to get a purchase order by ID "<id>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after post operation should be "<value>"

    Examples:
      | id    | expectedStatusCode | key  | value   |
      | 8     |         200        | id   | 8       |

  Scenario Outline: Delete purchase order by ID
    When I request to delete a purchase order by ID "<id>"
    Then I should get <expectedStatusCode> status code

    Examples:
      | id    | expectedStatusCode |
      | 8     |         200        |

  Scenario Outline: Get the inventory by status
    When I request to get the inventory by status
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after post operation should be "<value>"

    Examples:
      | expectedStatusCode | key  | value |
      |         200        | sold | 85    |


